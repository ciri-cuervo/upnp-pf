# README #

UPnP Port Forwarding is a Java app that lets you redirect ports from your router to your PC when UPnP is enabled.

* Current version: 1.0.0

### How do I get set up? ###

* git clone of the project
* go to the project directory and run the following Maven command
```
#!maven

mvn clean package eclipse:clean eclipse:eclipse
```

### Building the release ###

* go to the project directory and run the following Maven command
```
#!maven

mvn clean package -Denv=prod
```

### Download runnable jar ###

* [UPnP Port Forwarding v1.0] (https://bitbucket.org/ciri-cuervo/upnp-pf/downloads/upnp-pf.jar)
