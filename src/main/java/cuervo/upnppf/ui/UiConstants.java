package cuervo.upnppf.ui;

/**
 * @author Ciri
 * 
 */
public class UiConstants {

	public static final int MIN_DISCOVERY_TIMEOUT = 0;
	public static final int MAX_DISCOVERY_TIMEOUT = 30000;
	public static final int DEFAULT_DISCOVERY_TIMEOUT = 1000;

}
