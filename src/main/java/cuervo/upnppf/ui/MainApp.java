package cuervo.upnppf.ui;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Insets;
import java.util.Enumeration;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import cuervo.upnppf.core.DevicesDiscoverer;
import cuervo.upnppf.ui.listeners.DevicesPrinterObserver;
import cuervo.upnppf.ui.listeners.DiscoverDevicesActionListener;
import cuervo.upnppf.ui.listeners.ListDeviceEntriesActionListener;
import cuervo.upnppf.ui.listeners.NewPortMappingEntryActionListener;
import cuervo.upnppf.ui.listeners.PopupMouseListener;
import cuervo.upnppf.ui.listeners.RemoveSelectedActionListener;
import cuervo.upnppf.ui.model.CurrentData;
import cuervo.upnppf.ui.model.TableModelImpl;

public class MainApp {

	private static final int DEFAULT_DISCOVERY_TIMEOUT = 500;
	private static final Font DEFAULT_FONT = new Font("Tahoma", Font.PLAIN, 11);

	private JFrame frame;
	private DevicesDiscoverer discoverDevices;
	private CurrentData currentData;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainApp window = new MainApp();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MainApp() {
		discoverDevices = new DevicesDiscoverer(DEFAULT_DISCOVERY_TIMEOUT);
		currentData = new CurrentData();

		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			Enumeration<Object> keys = UIManager.getDefaults().keys();
			while (keys.hasMoreElements()) {
				Object key = keys.nextElement();
				Object value = UIManager.get(key);
				if (value instanceof javax.swing.plaf.FontUIResource) {
					UIManager.put(key, DEFAULT_FONT);
				}
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (UnsupportedLookAndFeelException e) {
			e.printStackTrace();
		}

		frame = new JFrame();
		frame.setResizable(false);
		frame.setBounds(100, 100, 759, 290);
		frame.setTitle("UPnP Port Forwarding");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JComboBox<String> comboBox = new JComboBox<String>();
		comboBox.setBounds(10, 10, 350, 23);
		frame.getContentPane().add(comboBox);

		JLabel lblStatus = new JLabel();
		lblStatus.setBounds(10, 246, 630, 15);
		frame.getContentPane().add(lblStatus);

		JPopupMenu popupMenu = new JPopupMenu("Menu");
		JMenuItem menuItem = new JMenuItem("To copy press Ctrl + C");
		popupMenu.add(menuItem);

		JTable table = new JTable(new TableModelImpl());
		table.addMouseListener(new PopupMouseListener(popupMenu));
		table.getTableHeader().setReorderingAllowed(false);
		TableModelImpl.setColumnWidths(table);

		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setBounds(10, 39, 630, 205);
		frame.getContentPane().add(scrollPane);

		JButton btnDiscoverDevices = new JButton();
		btnDiscoverDevices.setBounds(366, 8, 121, 25);
		btnDiscoverDevices.setText("Discover devices");
		btnDiscoverDevices.setMargin(new Insets(0, 0, 0, 0));
		frame.getContentPane().add(btnDiscoverDevices);

		JSpinner spinner = new JSpinner();
		spinner.setModel(new SpinnerNumberModel(UiConstants.DEFAULT_DISCOVERY_TIMEOUT, UiConstants.MIN_DISCOVERY_TIMEOUT,
				UiConstants.MAX_DISCOVERY_TIMEOUT, 10));
		spinner.setBounds(666, 10, 60, 22);
		spinner.setEditor(new JSpinner.NumberEditor(spinner, "#"));
		frame.getContentPane().add(spinner);

		JLabel lblDiscoveryTimeout = new JLabel();
		lblDiscoveryTimeout.setHorizontalAlignment(SwingConstants.RIGHT);
		lblDiscoveryTimeout.setBounds(540, 14, 100, 15);
		lblDiscoveryTimeout.setText("Discovery timeout");
		frame.getContentPane().add(lblDiscoveryTimeout);

		JButton btnRefreshTable = new JButton();
		btnRefreshTable.setBounds(650, 60, 93, 25);
		btnRefreshTable.setText("Refresh table");
		btnRefreshTable.setMargin(new Insets(0, 0, 0, 0));
		frame.getContentPane().add(btnRefreshTable);

		JButton btnAddForwarding = new JButton();
		btnAddForwarding.setBounds(650, 107, 93, 25);
		btnAddForwarding.setText("Add forwarding");
		btnAddForwarding.setMargin(new Insets(0, 0, 0, 0));
		frame.getContentPane().add(btnAddForwarding);

		JButton btnRemoveSelected = new JButton();
		btnRemoveSelected.setBounds(650, 140, 93, 25);
		btnRemoveSelected.setText("Remove");
		btnRemoveSelected.setMargin(new Insets(0, 0, 0, 0));
		frame.getContentPane().add(btnRemoveSelected);

		JButton btnExport = new JButton();
		btnExport.setBounds(650, 186, 93, 25);
		btnExport.setText("Export");
		btnExport.setMargin(new Insets(0, 0, 0, 0));
		frame.getContentPane().add(btnExport);

		JButton btnImport = new JButton();
		btnImport.setBounds(650, 219, 93, 25);
		btnImport.setText("Import");
		btnImport.setMargin(new Insets(0, 0, 0, 0));
		frame.getContentPane().add(btnImport);

		/*
		 * add listeners to components
		 */
		ListDeviceEntriesActionListener listDeviceEntries = new ListDeviceEntriesActionListener(comboBox, lblStatus, table, currentData);
		discoverDevices.addObserver(new DevicesPrinterObserver(comboBox, lblStatus, currentData, listDeviceEntries));
		btnDiscoverDevices.addActionListener(new DiscoverDevicesActionListener(discoverDevices, table, spinner));
		comboBox.addActionListener(listDeviceEntries);
		btnRefreshTable.addActionListener(listDeviceEntries);
		btnAddForwarding.addActionListener(new NewPortMappingEntryActionListener(frame, lblStatus, currentData, listDeviceEntries));
		btnRemoveSelected.addActionListener(new RemoveSelectedActionListener(table, lblStatus, currentData, listDeviceEntries));

	}

}
