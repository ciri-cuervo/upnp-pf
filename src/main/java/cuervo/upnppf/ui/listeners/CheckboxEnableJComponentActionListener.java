package cuervo.upnppf.ui.listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import javax.swing.JCheckBox;
import javax.swing.JComponent;

/**
 * @author Ciri
 * 
 */
public class CheckboxEnableJComponentActionListener implements ActionListener {

	private Set<JComponent> components;
	private boolean inverse;

	/**
	 * 
	 */
	public CheckboxEnableJComponentActionListener() {
		components = Collections.synchronizedSet(new HashSet<JComponent>());
		this.inverse = false;
	}

	/**
	 * 
	 */
	public CheckboxEnableJComponentActionListener(boolean inverse) {
		components = Collections.synchronizedSet(new HashSet<JComponent>());
		this.inverse = inverse;
	}

	/**
	 * 
	 * @param component
	 */
	public CheckboxEnableJComponentActionListener(JComponent component) {
		components = Collections.synchronizedSet(new HashSet<JComponent>());
		components.add(component);
		this.inverse = false;
	}

	/**
	 * 
	 * @param component
	 */
	public CheckboxEnableJComponentActionListener(JComponent component, boolean inverse) {
		components = Collections.synchronizedSet(new HashSet<JComponent>());
		components.add(component);
		this.inverse = inverse;
	}

	/**
	 * 
	 * @param component
	 * @return
	 */
	public boolean add(JComponent component) {
		return components.add(component);
	}

	/**
	 * 
	 * @param component
	 * @return
	 */
	public boolean remove(JComponent component) {
		return components.remove(component);
	}

	/**
	 * 
	 * @param component
	 * @return
	 */
	public boolean contains(JComponent component) {
		return components.contains(component);
	}

	/**
	 * 
	 * @return
	 */
	public boolean isEmpty() {
		return components.isEmpty();
	}

	/**
	 * 
	 * @return
	 */
	public int sise() {
		return components.size();
	}

	/**
	 * 
	 */
	public void clear() {
		components.clear();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	public void actionPerformed(ActionEvent e) {
		boolean chkSelected = ((JCheckBox) e.getSource()).isSelected();
		for (JComponent component : components) {
			component.setEnabled(!chkSelected && inverse || chkSelected && !inverse);
		}
	}

}
