package cuervo.upnppf.ui.listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import cuervo.upnppf.core.DevicesDiscoverer;
import cuervo.upnppf.ui.UiConstants;

/**
 * @author Ciri
 * 
 */
public class DiscoverDevicesActionListener implements ActionListener, Runnable {

	private static final Object lock = new Object();

	private DevicesDiscoverer devicesDiscoverer;
	private JTable table;
	private JSpinner spinner;
	private boolean running;

	/**
	 * 
	 * @param devicesDiscoverer
	 * @param table
	 * @param spinner
	 */
	public DiscoverDevicesActionListener(DevicesDiscoverer devicesDiscoverer, JTable table, JSpinner spinner) {
		this.devicesDiscoverer = devicesDiscoverer;
		this.table = table;
		this.spinner = spinner;
		this.running = false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	public void actionPerformed(ActionEvent e) {
		new Thread(this).start();
	}

	public void run() {

		synchronized (lock) {
			if (running) {
				return;
			}
			running = true;
		}

		((DefaultTableModel) table.getModel()).setRowCount(0);
		int spinnerValue = (Integer) spinner.getValue();
		devicesDiscoverer.setDiscoveryTimeout(Math.min(Math.max(spinnerValue, UiConstants.MIN_DISCOVERY_TIMEOUT),
				UiConstants.MAX_DISCOVERY_TIMEOUT));
		devicesDiscoverer.discoverDevices();

		synchronized (lock) {
			running = false;
		}

	}
}
