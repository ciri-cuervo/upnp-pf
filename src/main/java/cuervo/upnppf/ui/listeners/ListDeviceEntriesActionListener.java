package cuervo.upnppf.ui.listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import cuervo.upnppf.core.PortMappingEntryConstants;
import cuervo.upnppf.ui.model.CurrentData;
import net.sbbi.upnp.impls.InternetGatewayDevice;
import net.sbbi.upnp.messages.ActionResponse;

/**
 * @author Ciri
 * 
 */
public class ListDeviceEntriesActionListener implements ActionListener {

	private JComboBox<String> comboBox;
	private JLabel lblStatus;
	private JTable table;
	private CurrentData currentData;
	private boolean overrideStatusLabel;
	private boolean valid;

	/**
	 * 
	 * @param comboBox
	 * @param lblStatus
	 * @param table
	 * @param currentData
	 */
	public ListDeviceEntriesActionListener(JComboBox<String> comboBox, JLabel lblStatus, JTable table, CurrentData currentData) {
		this.comboBox = comboBox;
		this.lblStatus = lblStatus;
		this.table = table;
		this.currentData = currentData;
		this.overrideStatusLabel = true;
		this.valid = true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	public void actionPerformed(ActionEvent e) {
		if (!valid) {
			return;
		}

		int deviceIndex = comboBox.getSelectedIndex();
		if (deviceIndex < 0) {
			lblStatus.setText("No device selected.");
			return;
		}

		InternetGatewayDevice device = currentData.selectDevice(deviceIndex);
		Vector<String[]> portMappingEntries = new Vector<String[]>();

		try {
			int natMappingsCount = device.getNatMappingsCount();
			DefaultTableModel model = (DefaultTableModel) table.getModel();
			model.setRowCount(natMappingsCount);

			for (int i = 0; i < natMappingsCount; i++) {
				ActionResponse portMappingEntry = device.getGenericPortMappingEntry(i);

				String[] portMappingEntryValues = new String[8];
				portMappingEntryValues[PortMappingEntryConstants.DESCRIPTION] = portMappingEntry
						.getOutActionArgumentValue("NewPortMappingDescription");
				portMappingEntryValues[PortMappingEntryConstants.ENABLED] = "1".equals(portMappingEntry
						.getOutActionArgumentValue("NewEnabled")) ? "Yes" : "No";
				portMappingEntryValues[PortMappingEntryConstants.REMOTE_HOST] = portMappingEntry.getOutActionArgumentValue("NewRemoteHost");
				portMappingEntryValues[PortMappingEntryConstants.EXTERNAL_PORT] = portMappingEntry
						.getOutActionArgumentValue("NewExternalPort");
				portMappingEntryValues[PortMappingEntryConstants.INTERNAL_PORT] = portMappingEntry
						.getOutActionArgumentValue("NewInternalPort");
				portMappingEntryValues[PortMappingEntryConstants.INTERNAL_CLIENT] = portMappingEntry
						.getOutActionArgumentValue("NewInternalClient");
				portMappingEntryValues[PortMappingEntryConstants.PROTOCOL] = portMappingEntry.getOutActionArgumentValue("NewProtocol");
				portMappingEntryValues[PortMappingEntryConstants.LEASE_DURATION] = portMappingEntry
						.getOutActionArgumentValue("NewLeaseDuration");

				table.setValueAt(false, i, 0);
				for (int j = 0; j < portMappingEntryValues.length; j++) {
					table.setValueAt(portMappingEntryValues[j], i, j + 1);
				}

				portMappingEntries.add(portMappingEntryValues);
			}

			currentData.setPortMappingEntries(portMappingEntries);

			if (overrideStatusLabel) {
				if (natMappingsCount == 0) {
					lblStatus.setText("No port mapping entries were found.");
				} else {
					lblStatus.setText("Port mapping entries successfully listed.");
				}
			}
		} catch (Exception e1) {
			lblStatus.setText("There was an error while getting entries from device.");
			e1.printStackTrace();
		}
	}

	/**
	 * @param overrideStatusLabel
	 *            the overrideStatusLabel to set
	 */
	public void setOverrideStatusLabel(boolean overrideStatusLabel) {
		this.overrideStatusLabel = overrideStatusLabel;
	}

	/**
	 * @param valid
	 *            the valid to set
	 */
	public void setValid(boolean valid) {
		this.valid = valid;
	}

}
