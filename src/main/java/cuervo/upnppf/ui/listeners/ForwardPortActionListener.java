package cuervo.upnppf.ui.listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.InetAddress;

import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JTextField;

import cuervo.upnppf.ui.model.CurrentData;
import net.sbbi.upnp.impls.InternetGatewayDevice;

/**
 * @author Ciri
 * 
 */
public class ForwardPortActionListener implements ActionListener {

	private CurrentData currentData;
	private JDialog dialog;
	private JLabel lblStatus;
	private JTextField txtDescription;
	private JTextField txtRemoteHost;
	private JTextField txtExternalPort;
	private JTextField txtInternalClient;
	private JTextField txtInternalPort;
	private JTextField txtleaseTime;
	private JComboBox<String> comboBoxProt;
	private JCheckBox chkAllRemote;
	private JCheckBox chkSamePort;
	private ListDeviceEntriesActionListener listDeviceEntries;

	public ForwardPortActionListener(CurrentData currentData, JLabel lblStatus, JDialog dialog, JTextField txtDescription,
			JTextField txtRemoteHost, JTextField txtExternalPort, JTextField txtInternalClient, JTextField txtInternalPort,
			JTextField txtleaseTime, JComboBox<String> comboBoxProt, JCheckBox chkAllRemote, JCheckBox chkSamePort,
			ListDeviceEntriesActionListener listDeviceEntries) {
		this.currentData = currentData;
		this.dialog = dialog;
		this.lblStatus = lblStatus;
		this.txtDescription = txtDescription;
		this.txtRemoteHost = txtRemoteHost;
		this.txtExternalPort = txtExternalPort;
		this.txtInternalClient = txtInternalClient;
		this.txtInternalPort = txtInternalPort;
		this.txtleaseTime = txtleaseTime;
		this.comboBoxProt = comboBoxProt;
		this.chkAllRemote = chkAllRemote;
		this.chkSamePort = chkSamePort;
		this.listDeviceEntries = listDeviceEntries;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	public void actionPerformed(ActionEvent e) {
		try {
			InternetGatewayDevice device = currentData.getDevice();

			String description = (txtDescription.getText() == null) ? "" : txtDescription.getText();
			String internalClient = InetAddress.getByName(txtInternalClient.getText()).getHostAddress();

			int externalPort = -1;
			try {
				externalPort = Integer.valueOf(txtExternalPort.getText());
			} catch (NumberFormatException e1) {
			}

			int internalPort = -1;
			try {
				internalPort = Integer.valueOf(txtInternalPort.getText());
			} catch (NumberFormatException e1) {
			}

			int leaseTime = Math.max(Integer.valueOf(txtleaseTime.getText()), 0);
			String protocol = (String) comboBoxProt.getSelectedItem();

			String remoteHost;
			if (chkAllRemote.isSelected()) {
				remoteHost = null;
			} else {
				remoteHost = InetAddress.getByName(txtRemoteHost.getText()).getHostAddress();
			}

			if (chkSamePort.isSelected()) {
				externalPort = internalPort;
			}

			if ("Both".equalsIgnoreCase(protocol)) {
				device.addPortMapping(description + " TCP", remoteHost, internalPort, externalPort, internalClient, leaseTime, "TCP");
				device.addPortMapping(description + " UDP", remoteHost, internalPort, externalPort, internalClient, leaseTime, "UDP");
			} else {
				device.addPortMapping(description + " " + protocol, remoteHost, internalPort, externalPort, internalClient, leaseTime,
						protocol);
			}
			lblStatus.setText("Entries successfully added to device.");
		} catch (Exception e1) {
			lblStatus.setText("There was an error while adding entries to device: " + e1.getClass().getSimpleName() + " - "
					+ e1.getMessage());
			e1.printStackTrace();
		}

		dialog.dispose();

		listDeviceEntries.setOverrideStatusLabel(false);
		listDeviceEntries.actionPerformed(e);
		listDeviceEntries.setOverrideStatusLabel(true);
	}

}
