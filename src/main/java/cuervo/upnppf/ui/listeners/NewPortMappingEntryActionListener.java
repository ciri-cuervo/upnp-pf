/**
 * 
 */
package cuervo.upnppf.ui.listeners;

import java.awt.Font;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.InetAddress;
import java.net.UnknownHostException;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import cuervo.upnppf.ui.model.CurrentData;
import cuervo.upnppf.ui.model.FixedSizeDocument;

/**
 * @author Ciri
 * 
 */
public class NewPortMappingEntryActionListener implements ActionListener {

	private static int WIDTH = 455;
	private static int HEIGHT = 220;

	private JFrame frame;
	private JLabel lblStatus;
	private CurrentData currentData;
	private ListDeviceEntriesActionListener listDeviceEntries;

	public NewPortMappingEntryActionListener(JFrame frame, JLabel lblStatus, CurrentData currentData,
			ListDeviceEntriesActionListener listDeviceEntries) {
		this.frame = frame;
		this.lblStatus = lblStatus;
		this.currentData = currentData;
		this.listDeviceEntries = listDeviceEntries;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	public void actionPerformed(ActionEvent e) {

		if (currentData.getDevice() == null) {
			lblStatus.setText("No device selected.");
			return;
		}

		String netIp = currentData.getDevice().getIGDRootDevice().getURLBase().getHost();
		if (netIp == null) {
			netIp = "";
		} else {
			int pos = netIp.lastIndexOf(".");
			if (pos > -1) {
				netIp = netIp.substring(0, pos + 1);
			}
		}

		String localhostName = "";
		String localhostAddress = netIp;
		try {
			localhostName = InetAddress.getLocalHost().getHostName();
			for (InetAddress inetAddress : InetAddress.getAllByName(localhostName)) {
				if (inetAddress.getHostAddress().startsWith(netIp)) {
					localhostAddress = inetAddress.getHostAddress();
				}
			}
		} catch (UnknownHostException e1) {
		}

		final JDialog dialog = new JDialog(frame);
		dialog.setModal(true);
		dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		dialog.setResizable(false);
		dialog.setBounds(frame.getX() + (frame.getWidth() - WIDTH) / 2, frame.getY() + (frame.getHeight() - HEIGHT) / 2, WIDTH, HEIGHT);
		dialog.setTitle("New port mapping entry");
		dialog.getContentPane().setLayout(null);

		JLabel lblDescription = new JLabel();
		lblDescription.setBounds(15, 15, 100, 18);
		lblDescription.setText("Entry description");
		dialog.getContentPane().add(lblDescription);

		JTextField txtDescription = new JTextField();
		txtDescription.setBounds(lblDescription.getX() - 5, lblDescription.getY() + lblDescription.getHeight() + 2, 150, 18);
		txtDescription.setDocument(new FixedSizeDocument(80));
		dialog.getContentPane().add(txtDescription);

		JLabel lblLocalhost = new JLabel();
		lblLocalhost.setBounds(lblDescription.getX() + lblDescription.getWidth(), lblDescription.getY(), 325, 18);
		lblLocalhost.setText("Current localhost address: " + localhostAddress);
		lblLocalhost.setHorizontalAlignment(SwingConstants.RIGHT);
		dialog.getContentPane().add(lblLocalhost);

		JLabel lblRemoteHost = new JLabel();
		lblRemoteHost.setBounds(15, 65, 100, 18);
		lblRemoteHost.setText("Remote host");
		dialog.getContentPane().add(lblRemoteHost);

		JLabel lblExtPort = new JLabel();
		lblExtPort.setBounds(lblRemoteHost.getX() + lblRemoteHost.getWidth() + 2, lblRemoteHost.getY(), 68, 18);
		lblExtPort.setText("External port");
		dialog.getContentPane().add(lblExtPort);

		JLabel lblIntClient = new JLabel();
		lblIntClient.setBounds(lblExtPort.getX() + lblExtPort.getWidth() + 20, lblRemoteHost.getY(), 100, 18);
		lblIntClient.setText("Internal client");
		dialog.getContentPane().add(lblIntClient);

		JLabel lblIntPort = new JLabel();
		lblIntPort.setBounds(lblIntClient.getX() + lblIntClient.getWidth() + 2, lblRemoteHost.getY(), 68, 18);
		lblIntPort.setText("Internal port");
		dialog.getContentPane().add(lblIntPort);

		JLabel lblProtocol = new JLabel();
		lblProtocol.setBounds(lblIntPort.getX() + lblIntPort.getWidth() + 20, lblIntPort.getY(), 50, 18);
		lblProtocol.setText("Protocol");
		dialog.getContentPane().add(lblProtocol);

		JTextField txtRemoteHost = new JTextField();
		txtRemoteHost.setBounds(lblRemoteHost.getX() - 5, lblRemoteHost.getY() + lblRemoteHost.getHeight() + 2, 100, 18);
		txtRemoteHost.setDocument(new FixedSizeDocument(40));
		txtRemoteHost.setEnabled(false);
		dialog.getContentPane().add(txtRemoteHost);

		JLabel lblSep1 = new JLabel();
		lblSep1.setBounds(txtRemoteHost.getX() + txtRemoteHost.getWidth() + 2, txtRemoteHost.getY(), 8, 18);
		lblSep1.setHorizontalAlignment(SwingConstants.CENTER);
		lblSep1.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblSep1.setText(":");
		dialog.getContentPane().add(lblSep1);

		JTextField txtExternalPort = new JTextField();
		txtExternalPort.setBounds(lblSep1.getX() + lblSep1.getWidth() + 2, txtRemoteHost.getY(), 40, 18);
		txtExternalPort.setDocument(new FixedSizeDocument(5));
		txtExternalPort.setEnabled(false);
		dialog.getContentPane().add(txtExternalPort);

		JLabel lblMapsTo = new JLabel();
		lblMapsTo.setBounds(txtExternalPort.getX() + txtExternalPort.getWidth() + 8, txtRemoteHost.getY(), 20, 18);
		lblMapsTo.setHorizontalAlignment(SwingConstants.CENTER);
		lblMapsTo.setFont(new Font("Times New Roman", Font.BOLD, 14));
		lblMapsTo.setText("\u2192");
		dialog.getContentPane().add(lblMapsTo);

		JTextField txtInternalClient = new JTextField();
		txtInternalClient.setBounds(lblMapsTo.getX() + lblMapsTo.getWidth() + 8, txtRemoteHost.getY(), 100, 18);
		txtInternalClient.setDocument(new FixedSizeDocument(40));
		txtInternalClient.setText(localhostAddress);
		dialog.getContentPane().add(txtInternalClient);

		JLabel lblSep2 = new JLabel();
		lblSep2.setBounds(txtInternalClient.getX() + txtInternalClient.getWidth() + 2, txtRemoteHost.getY(), 8, 18);
		lblSep2.setHorizontalAlignment(SwingConstants.CENTER);
		lblSep2.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblSep2.setText(":");
		dialog.getContentPane().add(lblSep2);

		JTextField txtInternalPort = new JTextField();
		txtInternalPort.setBounds(lblSep2.getX() + lblSep2.getWidth() + 2, txtRemoteHost.getY(), 40, 18);
		txtInternalPort.setDocument(new FixedSizeDocument(5));
		dialog.getContentPane().add(txtInternalPort);

		JComboBox<String> comboBox = new JComboBox<String>();
		comboBox.setBounds(txtInternalPort.getX() + txtInternalPort.getWidth() + 41, txtRemoteHost.getY(), 48, 18);
		comboBox.addItem("Both");
		comboBox.addItem("TCP");
		comboBox.addItem("UDP");
		dialog.getContentPane().add(comboBox);

		JCheckBox chkAllRemote = new JCheckBox();
		chkAllRemote.setBounds(txtRemoteHost.getX(), txtRemoteHost.getY() + txtRemoteHost.getHeight() + 10, 18, 18);
		chkAllRemote.setSelected(true);
		chkAllRemote.setText("All remote hosts");
		chkAllRemote.addActionListener(new CheckboxEnableJComponentActionListener(txtRemoteHost, true));
		dialog.getContentPane().add(chkAllRemote);

		JLabel lblAllRemote = new JLabel();
		lblAllRemote.setBounds(chkAllRemote.getX() + chkAllRemote.getWidth() + 4, txtRemoteHost.getY() + txtRemoteHost.getHeight() + 10,
				88, 18);
		lblAllRemote.setText("All remote hosts");
		dialog.getContentPane().add(lblAllRemote);

		JCheckBox chkSamePort = new JCheckBox();
		chkSamePort.setBounds(lblAllRemote.getX() + lblAllRemote.getWidth() + 0, txtRemoteHost.getY() + txtRemoteHost.getHeight() + 10, 18,
				18);
		chkSamePort.setSelected(true);
		chkSamePort.setText("Same external port than internal");
		chkSamePort.addActionListener(new CheckboxEnableJComponentActionListener(txtExternalPort, true));
		dialog.getContentPane().add(chkSamePort);

		JLabel lblSamePort = new JLabel();
		lblSamePort.setBounds(chkSamePort.getX() + chkSamePort.getWidth() + 4, txtRemoteHost.getY() + txtRemoteHost.getHeight() + 10, 200,
				18);
		lblSamePort.setText("Same external port than internal");
		dialog.getContentPane().add(lblSamePort);

		JTextField txtleaseTime = new JTextField();
		txtleaseTime.setBounds(20, 159, 50, 18);
		txtleaseTime.setDocument(new FixedSizeDocument(8));
		txtleaseTime.setText("0");
		txtleaseTime.setHorizontalAlignment(SwingConstants.CENTER);
		dialog.getContentPane().add(txtleaseTime);

		JLabel lblleaseTime = new JLabel();
		lblleaseTime.setBounds(txtleaseTime.getX() + txtleaseTime.getWidth() + 10, txtleaseTime.getY() - 5, 100, 30);
		lblleaseTime.setText("<html>Lease time<br>(0 means for ever)</html>");
		dialog.getContentPane().add(lblleaseTime);

		JButton btnOk = new JButton();
		btnOk.setBounds(236, 155, 93, 25);
		btnOk.setText("Ok");
		btnOk.setMargin(new Insets(0, 0, 0, 0));
		btnOk.addActionListener(new ForwardPortActionListener(currentData, lblStatus, dialog, txtDescription, txtRemoteHost,
				txtExternalPort, txtInternalClient, txtInternalPort, txtleaseTime, comboBox, chkAllRemote, chkSamePort, listDeviceEntries));
		dialog.getContentPane().add(btnOk);

		JButton btnCancel = new JButton();
		btnCancel.setBounds(btnOk.getX() + btnOk.getWidth() + 10, btnOk.getY(), 93, 25);
		btnCancel.setText("Cancel");
		btnCancel.setMargin(new Insets(0, 0, 0, 0));
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dialog.dispose();
			}
		});
		dialog.getContentPane().add(btnCancel);

		dialog.setVisible(true);
	}

}
