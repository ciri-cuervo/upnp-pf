package cuervo.upnppf.ui.listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;
import javax.swing.JTable;

import cuervo.upnppf.core.PortMappingEntryConstants;
import cuervo.upnppf.ui.model.CurrentData;
import net.sbbi.upnp.impls.InternetGatewayDevice;

/**
 * @author Ciri
 * 
 */
public class RemoveSelectedActionListener implements ActionListener {

	private static final long DELAY_INTERVAL = 300;

	private JTable table;
	private JLabel lblStatus;
	private CurrentData currentData;
	private ListDeviceEntriesActionListener listDeviceEntries;

	/**
	 * 
	 * @param table
	 * @param lblStatus
	 * @param currentData
	 * @param listDeviceEntries
	 */
	public RemoveSelectedActionListener(JTable table, JLabel lblStatus, CurrentData currentData,
			ListDeviceEntriesActionListener listDeviceEntries) {
		this.table = table;
		this.lblStatus = lblStatus;
		this.currentData = currentData;
		this.listDeviceEntries = listDeviceEntries;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	public void actionPerformed(ActionEvent e) {

		InternetGatewayDevice device = currentData.getDevice();
		if (device == null) {
			lblStatus.setText("No device selected.");
			return;
		}

		boolean error = false;
		int selectedCount = 0;
		int rowCount = table.getRowCount();
		for (int i = 0; i < rowCount; i++) {
			boolean selected = (table.getValueAt(i, 0) == null) ? false : (Boolean) table.getValueAt(i, 0);
			if (selected) {
				String[] entry = currentData.selectPortMappingEntry(i);
				try {
					device.deletePortMapping(entry[PortMappingEntryConstants.REMOTE_HOST], Integer.valueOf(entry[PortMappingEntryConstants.EXTERNAL_PORT]), entry[PortMappingEntryConstants.PROTOCOL]);
				} catch (Exception e1) {
					lblStatus.setText("There was an error while deleting entries from device.");
					error = true;
					e1.printStackTrace();
				}
				selectedCount++;
			}
		}

		try {
			Thread.sleep(DELAY_INTERVAL);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}

		if (!error) {
			if (selectedCount > 0) {
				lblStatus.setText("Entries successfully deleted from device.");
			} else {
				lblStatus.setText("No entries selected.");
			}
		}

		listDeviceEntries.setOverrideStatusLabel(false);
		listDeviceEntries.actionPerformed(e);
		listDeviceEntries.setOverrideStatusLabel(true);
	}

}
