package cuervo.upnppf.ui.listeners;

import java.io.IOException;
import java.util.Arrays;
import java.util.Observable;
import java.util.Observer;
import java.util.Vector;

import javax.swing.JComboBox;
import javax.swing.JLabel;

import cuervo.upnppf.ui.model.CurrentData;
import net.sbbi.upnp.impls.InternetGatewayDevice;
import net.sbbi.upnp.messages.UPNPResponseException;

/**
 * @author Ciri
 * 
 */
public class DevicesPrinterObserver implements Observer {

	private JComboBox<String> combo;
	private JLabel label;
	private CurrentData currentData;
	private ListDeviceEntriesActionListener listDeviceEntries;

	/**
	 * 
	 * @param combo
	 * @param label
	 * @param currentData
	 * @param listDeviceEntries
	 */
	public DevicesPrinterObserver(JComboBox<String> combo, JLabel label, CurrentData currentData,
			ListDeviceEntriesActionListener listDeviceEntries) {
		this.combo = combo;
		this.label = label;
		this.currentData = currentData;
		this.listDeviceEntries = listDeviceEntries;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.Observer#update(java.util.Observable, java.lang.Object)
	 */
	public void update(Observable o, Object arg) {
		if (arg == null) {
			label.setText("Discovering devices...");
			return;
		}

		currentData.setDevices(new Vector<InternetGatewayDevice>(Arrays.asList((InternetGatewayDevice[]) arg)));

		listDeviceEntries.setValid(false);
		combo.removeAllItems();

		int i = 1;
		for (InternetGatewayDevice device : currentData.getDevices()) {

			String manufacturer = " - " + device.getIGDRootDevice().getManufacturer();
			String host = " - " + device.getIGDRootDevice().getURLBase().getHost();

			String externalIpAddress = "";
			try {
				externalIpAddress = " - " + device.getExternalIPAddress();
			} catch (UPNPResponseException e) {
			} catch (IOException e) {
			}

			combo.addItem(String.valueOf(i++) + manufacturer + host + externalIpAddress);
		}

		if (currentData.getDevices().size() == 1) {
			listDeviceEntries.setValid(true);
			combo.setSelectedIndex(0);
		}

		if (currentData.getDevices().size() > 1) {
			listDeviceEntries.setValid(true);
			combo.setPopupVisible(true);
		}

		label.setText(String.valueOf(currentData.getDevices().size()) + " device" + (currentData.getDevices().size() == 1 ? "" : "s")
				+ " discovered");

	}

}
