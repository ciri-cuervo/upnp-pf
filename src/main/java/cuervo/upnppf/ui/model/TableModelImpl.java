/**
 * 
 */
package cuervo.upnppf.ui.model;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 * @author Ciri
 * 
 */
public class TableModelImpl extends DefaultTableModel {

	private static final long serialVersionUID = 1L;

	private static final String[] PORT_MAPPING_HEADERS = new String[] { "", "Description", "Enabled", "Remote host", "Ext. port",
			"Int. port", "Internal client", "Protocol", "Lease" };

	private static final int[] PORT_MAPPING_WIDTHS = new int[] { 20, 178, 48, 96, 56, 56, 96, 48, 48 };

	/**
	 * 
	 */
	public TableModelImpl() {
		super(PORT_MAPPING_HEADERS, 0);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.table.AbstractTableModel#getColumnClass(int)
	 */
	@Override
	public Class<?> getColumnClass(int columnIndex) {
		if (columnIndex == 0) {
			return Boolean.class;
		}
		return String.class;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.table.DefaultTableModel#isCellEditable(int, int)
	 */
	@Override
	public boolean isCellEditable(int row, int column) {
		if (column == 0) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * @param table
	 */
	public static void setColumnWidths(JTable table) {
		for (int i = 0; i < PORT_MAPPING_HEADERS.length; i++) {
			table.getColumnModel().getColumn(i).setPreferredWidth(PORT_MAPPING_WIDTHS[i]);
		}
	}

}
