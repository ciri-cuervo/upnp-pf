/**
 * 
 */
package cuervo.upnppf.ui.model;

import java.util.Vector;

import net.sbbi.upnp.impls.InternetGatewayDevice;

/**
 * @author Ciri
 * 
 */
public class CurrentData {

	private Vector<InternetGatewayDevice> devices;
	private InternetGatewayDevice device;
	private int deviceIndex;
	private Vector<String[]> portMappingEntries;
	private String[] portMappingEntry;
	private int portMappingEntryIndex;

	public CurrentData() {
		this.devices = null;
		this.device = null;
		this.deviceIndex = -1;
		this.portMappingEntries = null;
		this.portMappingEntry = null;
		this.portMappingEntryIndex = -1;
	}

	/**
	 * 
	 * @param devices
	 */
	public void setDevices(Vector<InternetGatewayDevice> devices) {
		this.devices = devices;
		this.device = null;
		this.deviceIndex = -1;
		this.portMappingEntries = null;
		this.portMappingEntry = null;
		this.portMappingEntryIndex = -1;
	}

	/**
	 * 
	 * @param deviceIndex
	 * @return
	 */
	public InternetGatewayDevice selectDevice(int deviceIndex) {

		if (deviceIndex < 0 || deviceIndex >= this.devices.size()) {
			throw new RuntimeException("Incorrect index.");
		}

		this.device = this.devices.get(deviceIndex);
		this.deviceIndex = deviceIndex;
		this.portMappingEntries = null;
		this.portMappingEntry = null;
		this.portMappingEntryIndex = -1;

		return this.device;
	}

	/**
	 * 
	 * @param device
	 * @return
	 */
	public int selectDevice(InternetGatewayDevice device) {

		if (!this.devices.contains(device)) {
			throw new RuntimeException("The device wasn't found.");
		}

		this.device = device;
		this.deviceIndex = this.devices.indexOf(device);
		this.portMappingEntries = null;
		this.portMappingEntry = null;
		this.portMappingEntryIndex = -1;

		return this.deviceIndex;
	}

	/**
	 * 
	 * @param portMappingEntries
	 */
	public void setPortMappingEntries(Vector<String[]> portMappingEntries) {
		this.portMappingEntries = portMappingEntries;
		this.portMappingEntry = null;
		this.portMappingEntryIndex = -1;
	}

	/**
	 * 
	 * @param portMappingEntryIndex
	 * @return
	 */
	public String[] selectPortMappingEntry(int portMappingEntryIndex) {

		if (portMappingEntryIndex < 0 || portMappingEntryIndex >= this.portMappingEntries.size()) {
			throw new RuntimeException("Incorrect index.");
		}

		this.portMappingEntry = this.portMappingEntries.get(portMappingEntryIndex);
		this.portMappingEntryIndex = portMappingEntryIndex;

		return this.portMappingEntry;
	}

	/**
	 * 
	 * @param portMappingEntry
	 * @return
	 */
	public int selectPortMappingEntry(String[] portMappingEntry) {

		if (!this.portMappingEntries.contains(portMappingEntry)) {
			throw new RuntimeException("The device wasn't found.");
		}

		this.portMappingEntry = portMappingEntry;
		this.portMappingEntryIndex = this.portMappingEntries.indexOf(portMappingEntry);
		
		return this.portMappingEntryIndex;
	}

	/**
	 * @return the devices
	 */
	public Vector<InternetGatewayDevice> getDevices() {
		return devices;
	}

	/**
	 * @return the device
	 */
	public InternetGatewayDevice getDevice() {
		return device;
	}

	/**
	 * @return the deviceIndex
	 */
	public int getDeviceIndex() {
		return deviceIndex;
	}

	/**
	 * @return the portMappingEntries
	 */
	public Vector<String[]> getPortMappingEntries() {
		return portMappingEntries;
	}

	/**
	 * @return the portMappingEntry
	 */
	public String[] getPortMappingEntry() {
		return portMappingEntry;
	}

	/**
	 * @return the portMappingEntryIndex
	 */
	public int getPortMappingEntryIndex() {
		return portMappingEntryIndex;
	}

}
