package cuervo.upnppf.core;

import java.io.IOException;
import java.util.Observable;

import net.sbbi.upnp.impls.InternetGatewayDevice;

/**
 * @author Ciri
 * 
 */
public class DevicesDiscoverer extends Observable implements Runnable {

	private static final long SLEEP_INTERVAL = 2000;
	private static final long DISCOVERY_INTERVAL = 300;

	private InternetGatewayDevice[] devices;
	private boolean daemonDiscovery;
	private int discoveryTimeout;
	private long lastDiscoveryTime;

	public DevicesDiscoverer(int discoveryTimeout) {
		this.devices = null;
		this.daemonDiscovery = true;
		this.discoveryTimeout = discoveryTimeout;
		this.lastDiscoveryTime = 0;
	}

	public void run() {

		while (true) {

			this.discoverDevices();

			try {
				Thread.sleep(SLEEP_INTERVAL);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

	}

	/**
	 * 
	 */
	public synchronized void startDaemonDiscovery() {
		daemonDiscovery = true;
		this.notifyAll();
	}

	/**
	 * 
	 */
	public synchronized void stopDaemonDiscovery() {
		daemonDiscovery = false;
	}

	/**
	 * 
	 */
	public void discoverDevices() {

		synchronized (this) {

			if (!daemonDiscovery) {
				try {
					this.wait();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}

			// notify observers that the discovery has started
			this.setChanged();
			this.notifyObservers(null);

			long diff = System.currentTimeMillis() - lastDiscoveryTime;
			if (diff < DISCOVERY_INTERVAL) {
				try {
					Thread.sleep(DISCOVERY_INTERVAL - diff);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}

			try {
				devices = null;
				devices = InternetGatewayDevice.getDevices(discoveryTimeout);
			} catch (IOException e) {
				e.printStackTrace();
			}

			if (devices == null) {
				devices = new InternetGatewayDevice[0];
			}

			lastDiscoveryTime = System.currentTimeMillis();
		}

		this.setChanged();
		this.notifyObservers(devices);
	}

	/**
	 * 
	 * @return
	 */
	public boolean isDaemonDiscovery() {
		return daemonDiscovery;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isDiscovering() {
		return daemonDiscovery;
	}

	/**
	 * 
	 * @return
	 */
	public synchronized InternetGatewayDevice[] getDevices() {
		return devices;
	}

	/**
	 * 
	 * @return
	 */
	public synchronized int getDevicesNum() {
		return devices.length;
	}

	/**
	 * @return the discoveryTimeout
	 */
	public int getDiscoveryTimeout() {
		return discoveryTimeout;
	}

	/**
	 * @param discoveryTimeout
	 *            the discoveryTimeout to set
	 */
	public void setDiscoveryTimeout(int discoverTimeout) {
		this.discoveryTimeout = discoverTimeout;
	}

}
