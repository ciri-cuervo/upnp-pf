package cuervo.upnppf.core;

/**
 * @author Ciri
 * 
 */
public class PortMappingEntryConstants {

	public static final int DESCRIPTION = 0;
	public static final int ENABLED = 1;
	public static final int REMOTE_HOST = 2;
	public static final int EXTERNAL_PORT = 3;
	public static final int INTERNAL_PORT = 4;
	public static final int INTERNAL_CLIENT = 5;
	public static final int PROTOCOL = 6;
	public static final int LEASE_DURATION = 7;

}
